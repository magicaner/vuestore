import path from 'node:path';
import url from 'node:url';
import type { Module } from '@nuxt/types';
import type { NuxtRouteConfig } from '@nuxt/types/config/router';

const moduleDir = path.dirname(url.fileURLToPath(import.meta.url));

const moduleRoutes : NuxtRouteConfig = {
  name: 'notifications-index',
  path: '/notifications',
  component: path.resolve(moduleDir, 'pages/Index.vue'),
  meta: { titleLabel: 'Notification Center' }
};

const nuxtModule : Module = function () {
  this.extendRoutes((routes: NuxtRouteConfig[]) => {
    routes.unshift(moduleRoutes);
  });
};

export default nuxtModule;
