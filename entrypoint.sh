#!/bin/bash
if [[ $NODE_ENV == "local" ]]; then
    if [[ ! -d "node_modules" ]]; then
        yarn install
    fi
    rm -f tsconfig.tsbuildinfo
    yarn run dev
else
    if [[ ! -d "node_modules" ]]; then
        yarn install
    fi

    yarn run build
    yarn run start
fi
